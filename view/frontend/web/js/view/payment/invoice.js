define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        console.log('Loading payment Invoice');
        rendererList.push(
            {
                type: 'invoice',
                component: 'Kowal_InvoicePayment/js/view/payment/method-renderer/invoice-method'
            }
        );
        return Component.extend({});
    }
);