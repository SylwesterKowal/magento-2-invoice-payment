<?php


namespace Kowal\InvoicePayment\Plugin\Frontend\Magento\Payment\Model;

/**
 * Class MethodList
 *
 * @package Kowal\InvoicePayment\Plugin\Frontend\Magento\Payment\Model
 */
class MethodList
{

    /**
     * @var \Magento\Payment\Helper\Data
     */
    protected $paymentHelper;

    /**
     * @param \Magento\Payment\Helper\Data $paymentHelper
     *
     */
    public function __construct(
        \Magento\Payment\Helper\Data $paymentHelper
    )
    {
        $this->paymentHelper = $paymentHelper;
    }

    public function aroundGetAvailableMethods(
        \Magento\Payment\Model\MethodList $subject,
        \Closure $proceed,
        $quote = null
    ) {
        //Your plugin code
//        $result = $proceed($quote);
//        return $result;

        $paymentMethods = $proceed($quote);
        $store = $quote ? $quote->getStoreId() : null;
        $storePaymentMethods = $this->paymentHelper->getStoreMethods($store, $quote);

        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $code = $om->create('Magento\Checkout\Model\Cart')->getQuote()
            ->getShippingAddress()->getShippingMethod();

        $tax_number = '';

        $retArray = array();
        foreach ($storePaymentMethods as $method) {
            foreach ($paymentMethods as $methodFromList) {
                if ($method->getCode() == $methodFromList->getCode()) {

//                    if (trim($tax_number) == '' && $methodFromList->getCode() != 'invoice'  ) {
//                        $retArray[] = $method;
//                    }

                }
            }
        }
        return $retArray;
    }
}

