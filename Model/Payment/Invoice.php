<?php


namespace Kowal\InvoicePayment\Model\Payment;

/**
 * Class Invoice
 *
 * @package Kowal\InvoicePayment\Model\Payment
 */
class Invoice extends \Magento\Payment\Model\Method\AbstractMethod
{

    protected $_code = "invoice";
    protected $_isOffline = true;

    public function isAvailable(
        \Magento\Quote\Api\Data\CartInterface $quote = null
    ) {
        return parent::isAvailable($quote);
    }
}

